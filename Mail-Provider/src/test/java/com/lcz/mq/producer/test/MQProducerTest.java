package com.lcz.mq.producer.test;

import com.lcz.mq.provider.bean.MailBean;
import com.lcz.mq.provider.handler.MQProducer;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * ActiveMQ测试启动类
 *
 */
public class MQProducerTest {
	private static final Log log = LogFactory.getLog(MQProducerTest.class);

	public static void main(String[] args) {
		try {
			ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("classpath:spring/spring-context.xml");
			context.start();
			MQProducer mqProducer = (MQProducer) context.getBean("mqProducer");
			// 邮件发送
			MailBean mail = new MailBean();
			mail.setTo("1090061055@qq.com");
			mail.setSubject("ActiveMQ测试");
			mail.setContent("通过ActiveMQ异步发送邮件！");
			mqProducer.sendMessage(mail);
			context.stop();
		} catch (Exception e) {
			log.error("==>MQ context start error:", e);
			System.exit(0);
		} finally {
			log.info("===>System.exit");
			System.exit(0);
		}
	}
}
