package com.lcz.mq.provider.handler;

import com.alibaba.fastjson.JSONObject;
import com.lcz.mq.provider.bean.MailBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.stereotype.Service;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;

/**
 * MQ消息生产者
 * Created by luchunzhou on 2018/3/12.
 */
@Service("mqProducer")
public class MQProducer {

    @Autowired
    private JmsTemplate activeMqJmsTemplate;

    /**
     * 发送消息.
     *
     * @param mailBean
     */
    public void sendMessage(final MailBean mailBean) {
        activeMqJmsTemplate.send(new MessageCreator() {
            @Override
            public Message createMessage(Session session) throws JMSException {
                return session.createTextMessage(JSONObject.toJSONString(mailBean));
            }
        });

    }

}
