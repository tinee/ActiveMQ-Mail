package com.lcz.mq.consumer.test;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * ActiveMQ启动测试类
 */
public class MQConsumerTest {
    private static final Log log = LogFactory.getLog(MQConsumerTest.class);

    public static void main(String[] args) {
        try {
            ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("classpath:spring/spring-context.xml");
            context.start();
        } catch (Exception e) {
            log.error("==>MQ context start error:", e);
            System.exit(0);
        }
    }
}
